# coding: utf8
import os
from flask import Flask
from importlib import import_module

# 定义应用
app = Flask(__name__)

# 根目录模块文件夹名称
top_dirnames = next(os.walk(__path__[0]))[1]
modules = [m for m in top_dirnames if not m.startswith("__")]

# 自动导入所有模块
for module in modules:
    try:
        import_module(f"apps.{module}")
    except ImportError as ierror:
        print("自动导入模块", ierror)
